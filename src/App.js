// Import FirebaseAuth and firebase.
import React, { useState, useEffect } from 'react';
import "./App.css"
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import firebase from 'firebase';
import logo from './image/logo.png'

// Configure Firebase.
const config = {
  apiKey: "AIzaSyCvmYlznW5Lj5bnRH5ttTuSfSVqFYHZnNM",
  authDomain: "login-325505.firebaseapp.com",
  projectId: "gmail-login-325505",
  storageBucket: "gmail-login-325505.appspot.com",
  messagingSenderId: "139032792100",
  appId: "1:139032792100:web:b345636062ca2679801cb1"
};
firebase.initializeApp(config);

if (!firebase.apps.length) {
  firebase.initializeApp({});
}else {
  firebase.app(); // if already initialized, use that one
}

// Configure FirebaseUI.
const uiConfig = {
  signInFlow: 'popup',
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
  ],
  callbacks: {
    signInSuccess: () => false
  }
};

function App() {
const [SignIn, setSignIn] = useState(false)

useEffect(  () => { 
   firebase.auth().onAuthStateChanged(user => {
    setSignIn({ SignIn: !!user });
    setSignIn(user);

    // console.log("user", user)
  });
}, []);
// console.log("cek",SignIn)


  return (
    <>
    <div className='App-header'>

      {SignIn === null ? 
      <div style={{alignItems:'center', textAlign:'center' }}>
      <h1 style={{marginTop:'-10rem',}}>Welcome</h1>
      <p>Please sign-in</p>
      <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
      </div>
    :
    ( 
      SignIn === null ? null :  
    <div>
        <div className='Container' style={{marginTop:'-10rem',boxShadow:'1px 1px 1px 15px #EA4335',borderRadius:'20px',backgroundColor:'white', width:'auto', height:'6rem', display:'flex'}}>
        <div className='Photo' >
          <img style={{borderRadius:'20px'}}
              alt={logo}
              src={SignIn?.photoURL}
            />
        </div>
        <div style={{alignItems:'center', justifyContent:'flex-start' ,display:'flex', flexDirection:'column'}}>
          <p style={{color:'black', margin:'1rem 1rem 0 1rem', fontSize:'1rem'}}>Welcome {SignIn?.displayName}</p>
          <p style={{color:'black', margin:'0.2rem 1rem 0.4rem 1rem', fontSize:'1rem'}}>{SignIn?.email}</p>

        <button style={{cursor: 'pointer',background:'#4285F4',padding:'0 1rem 0 1rem',color:'white',borderRadius:'20px',border:'none', height:'1.5rem'}} onClick={() => firebase.auth().signOut()}>Sign Out</button> 
        </div>
      </div>
    </div>
      
    )
  }
    </div>
    </>
  );
}

export default App